FROM node:14.16-slim
WORKDIR /ondunova_socket
COPY ./package.json /ondunova_socket
RUN npm install
COPY . /ondunova_socket
EXPOSE 5002
CMD ["node", "index.js"]