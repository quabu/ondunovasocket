const fetch = require('node-fetch');

let urlAPI = 'ondunova_api:5001'

module.exports = class SocketHttpComunication {
    constructor() {
    }

    async newOrderPickupAuto(orderPickup) {
        try {
            console.log("http://"+ urlAPI + "/newOrderPickupAuto")
            const response = await fetch("http://" + urlAPI + "/newOrderPickupAuto",
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(orderPickup)
                }).catch(function (e) {
                    throw e
                });
            console.log(`Response: ${response.status} ${response.statusText}`);
            var data = await response.json()
            return data
        } catch (error) {
            console.log("ERROR Order Pickup: ", error)
            throw error
        }
    }
}