const net = require('net');

var client = new net.Socket()


module.exports = {

    connect: function (host,port) {
        // Versión para servidor actual nakire (portatil windows)
        /* client.connect(
            2001,
            "192.168.9.101",
            () => {
                logger.info("Connected to server")
                //client.write("Hello, server!")
            }
        ) */
        // Versión para localhost
        client.connect(
            port,
            host,
            () => {
                console.log("Connected to server"+" "+ host+" "+ port)
            }
        )

        client.on("data", data => {
            console.log("*** DATA RECEIVED ***")
            console.log(data)
            var dataString = data.toString()
            if (dataString.length > 0) {
                console.log("has data")
                if (dataString == "ERROR" ) {
                    //checkear el string por si tengo q hacer alguna accion
                    //TODO
                    console.log("newOrderPickup")
                    
                } else {
                    console.log("newOrderPickup")
                   
                }
            } else {
                console.log("no data")
            }
        })

        client.on("close", () => {
            console.log("CLOSE::: ")
            console.log(host+" "+port)
            client.removeAllListeners()
            reconnect(host,port)
        })

        client.on("end", () => {
            console.log("END::: ")
            console.log(host+" "+port)
            client.removeAllListeners()
            reconnect(host,port)
        })

        client.on("error", (error) => {
            console.log("ERRORR::: ")
            console.log(host+" "+port)
            client.removeAllListeners()
            reconnect(host,port)
        })


        // function that reconnect the client to the server
        reconnect = (host,port) => {
            setTimeout(() => {
                client.removeAllListeners() // the important line that enables you to reopen a connection
                this.connect(host,port)
            }, 10000) // try reconnect every 2s
        }
    },
}