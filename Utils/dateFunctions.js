

module.exports = class dateFunctions {
    constructor() {}

    addHoursToDate(date, hours) {
        var newDate = new Date(new Date(date).setHours(date.getHours() + hours));
        return newDate
    }

    formatDate(date){
        var formatedDate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
        return formatedDate
    } 
}



