
const Net = require('net');

//Utils
var dateFunctions = require("./Utils/dateFunctions");


//Socket
var socket = require('./Infrastructure/socketController')


const server = new Net.Server();
var host = "172.16.21.7"
/* var host = "0.0.0.0" */
var port = 5002
var socketHttpComunication = require('./Infrastructure/socketHttpComunication')
var socketHttp = new socketHttpComunication()

server.listen(port, function() {
    console.log(`Server listening for connection requests on socket localhost:${port}`);
});

server.on('connection', function(socket) {
    console.log('A new connection has been established.');

    // Now that a TCP connection has been established, the server can send data to
    // the client by writing to its socket.
    socket.write('Hello, client.');

    // The server can also receive data from the client by reading from its socket.
    socket.on('data', async function(chunk) {
        let dataString = chunk.toString()
        console.log("String received:", dataString)
        if (dataString.length > 0) {
            console.log("has data")
            if (dataString.includes("ERROR")) {
                //checkear el string por si tengo q hacer alguna accion
                //TODO
                console.log("newOrderPickup ERROR")
                let dateF = new dateFunctions()
                let date = dateF.addHoursToDate(new Date(), 1)
                let formatedDate = dateF.formatDate(date)
                var orderPickup = {
                    orderPickup : {
                        numOf: 'ERROR-' + formatedDate,
                        jobNumber: null,
                        numPalet: 0,
                        quantity: 0,
                        origin: "Onduladora"
                    }   
                }

                let result = await socketHttp.newOrderPickupAuto(orderPickup).catch(function (e) {
                    console.log("************* ERROR ************")
                    throw e
                })
            } else {
                console.log("newOrderPickup")
                let jobNumber = "";
                var infoTrim = dataString.replace(" ", '').replace("R", "")
                var infoSplit = infoTrim.split("#")
                var numOf = infoSplit[0]

                if (numOf.includes("a") || numOf.includes("b")){
                    numOf = numOf.replace(/[^\d.-]/g, '');
                    jobNumber = numOf.replace(/[0-9]/g, '')
                }
                var numPalet = infoSplit[1]
                var quantity = infoSplit[3]
                
                var orderPickup = {
                    orderPickup : {
                        numOf: numOf,
                        jobNumber: jobNumber,
                        numPalet: numPalet,
                        quantity: quantity,
                        origin: "Onduladora"
                    }   
                }

                console.log(orderPickup)

                let result = await socketHttp.newOrderPickupAuto(orderPickup).catch(function (e) {
                    console.log("************* ERROR ************")
                    throw e
                })    
            }
            console.log("FINISHED")
        } else {
            console.log("no data")
        }
    });

    // When the client requests to end the TCP connection with the server, the server ends the connection.
    socket.on('end', function() {
        console.log('Closing connection with the client');
    });

    socket.on('error', function(err) {
        console.log(`Error: ${err}`);
    });
});